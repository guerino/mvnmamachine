/*
 */
package it.mariano.mvnmamachine.server;

import it.mariano.malog.MaLog;
import it.mariano.mvnmamachine.utility.Ut;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author maria
 */
public class Server {

    private static final String SERVER_CONF_NAME = "conf/server.properties";
    Properties prop = new Properties();

    /**
     * Server socket
     */
    ServerSocket ss = null;
    /**
     * executorService gestione clients
     */
    public static final ExecutorService EX_MNG_SERVER = Executors.newSingleThreadExecutor();
    public static ExecutorService exServer;
    /**
     * Logs
     */
    MaLog malog = new MaLog(getClass());

    {
        malog.setLevel(MaLog.LEVELS.VERBOSE);
    }

    /**
     * Server constructor
     *
     * @throws java.io.IOException
     */
    public Server() throws IOException {
        /**
         * initialize property file
         */

        prop.load(Ut.getRisorsa(getClass(),SERVER_CONF_NAME));
        /**
         * initialize executor pool clients
         */
        exServer = Executors.newFixedThreadPool(Integer.parseInt(prop.getProperty("pool")));
        /**
         * open the socket, if port not specified inside server.property then
         * 6363
         */
        ss = new ServerSocket(Integer.parseInt(prop.getProperty("port", "6363")));
        /**
         * the accept socket delay time
         */
        ss.setSoTimeout(1000 * Integer.parseInt(prop.getProperty("accept_delay", "5")));
        malog.info("Server start on port:" + prop.getProperty("port", "6363"));
    }

    /**
     * Server Up!
     *
     * @return
     */
    public Server goUp() {
        EX_MNG_SERVER.execute(new ServerManager());
        return this;
    }

    /**
     * Server Manager, single thread loop forever. Accept client and run a
     * thread connection.
     */
    final class ServerManager implements Runnable {

        @Override
        public void run() {
            for (;;) {
                try {
                    Socket sc = ss.accept();
                    exServer.execute(new sockServer(sc));
                } catch (IOException ex) {
                    if (ex instanceof SocketTimeoutException) {
                        try {
                            //Utile per eventuale stop del server
                            Thread.sleep(10);
                        } catch (InterruptedException ex1) {
                            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex1);
                        }
                    } else {
                        Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }

    /**
     * Socket handshake thread
     */
    final class sockServer implements Runnable {

        /**
         * Questa signorinella di Lista conterrà i comandi che il server penserà
         * a spedire al client connesso
         */
        List<String> comandi = new ArrayList<>();
        /**
         * Il program counter della lista comandi
         */
        int progCount=0;

        Socket soc;

        public sockServer(Socket soc) {
            this.soc = soc;
        }

        /**
         * Esecuzione di comando(a volte più di uno, se relativo al server stesso)
         * da mandare al client.
         */
        private String getOrExecCommand(){
            if (progCount>=comandi.size()) 
                progCount--;
            return comandi.get(progCount);
        }
        private void initializeCommands(String idCliente) {
            /**
             * Se non esiste il file comandi o c'è un errore in lettura si crea
             * una lista di un unico comand contenente "***ERR COMMAND
             * FILE READ ***. id-Client==>XXX" 
             * Cosi, monitorando il protocollo si vedrà chiaro che manca qualcosa.
             */
            Path cmdFile = Paths.get(Ut.PATH_USER.toString(), "command" + idCliente);

            try (Scanner scan = new Scanner(cmdFile)) {
                while (scan.hasNext()) {
                    comandi.add(scan.nextLine());
                }
            } catch (IOException ex) {
                malog.wtf("***ERROR***Command file non trovato o errore lettura:" + cmdFile.toString());
//                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                comandi.add("***ERR COMMAND FILE READ***. id-Client ==> "+idCliente);
            }
        }

        @Override
        public void run() {
            try (DataInputStream dis = new DataInputStream(soc.getInputStream());
                    DataOutputStream dos = new DataOutputStream(soc.getOutputStream())) {
                /**
                 * protocollo
                 */
                /**
                 * ricevo ID dal client(eventuali altri dati di intestazione
                 * pacchetto spediti solo all' inizio.
                 */
                String idClient = dis.readUTF();
                initializeCommands(idClient);
                /**
                 * fine lettura dati intestazione client
                 */
                malog.verbose("From Client(id):" + idClient);
                for (;;) {
                    dos.writeUTF(getOrExecCommand());              //Saluto il client o lo comando
                    String ack = dis.readUTF();         //Mi aspetto un "ACK" per andare avanti, altrimenti mi ripeto
                    malog.verbose("From Client(" + idClient + "):" + ack);
                    if (!ack.toUpperCase().equals("ACK")) {
                        malog.wtf("Niente ACK, il Client non mi capisce...\nricevuto:" + ack);
                    }else{
                        progCount++;
                    }
                    try {
                        Thread.currentThread().sleep(Integer.parseInt(prop.getProperty("handshake_delay", "1")));
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                        return;
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public static void main(String[] args) {
        try {
            Server server = new Server().goUp();
//            new Client("_1").goUp();
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
