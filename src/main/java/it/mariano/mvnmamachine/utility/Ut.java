/*
 */
package it.mariano.mvnmamachine.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author ma
 */
public class Ut {

    public static final Path PATH_USER = Paths.get(System.getProperty("user.home") + "/.MvnMaMachine/");
    /**
     * Legge la risorsa come InputStream da un fileSystem ${user.home}/.mvnmamachine
     * Se non la trova la legge dallo Jar usando la getResourceAsStream()
     * @param c
     * @param fileRisorsa
     * @return 
     */
    public static InputStream getRisorsa(Class c, String fileRisorsa) throws FileNotFoundException{
        File file = new File(PATH_USER+"/"+fileRisorsa);
        if (file.exists()) return new FileInputStream(file);
        InputStream is = c.getClassLoader().getResourceAsStream(fileRisorsa);
        if (is == null){
            System.out.println("FATAL ERROR: Missing resource:"+fileRisorsa);
            throw new FileNotFoundException(fileRisorsa);
        }
        return is;
    }
/**
     * Copy a file from source to destination.
     *
     * @param source
     *        the source
     * @param destination
     *        the destination
     * @return True if succeeded , False if not
     */
    public static boolean copy(InputStream source , String destination) {
        boolean success = true;

        System.out.println("Copying ->" + source + "\n\tto ->" + destination);

        try {
            Files.copy(source, Paths.get(destination), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ex) {
            System.out.println("[Ut.copy-ERROR] dest-file:"+destination);
            System.out.println(ex);
            success = false;
        }
        System.out.println("[Ut.copy-SUCCESS] dest-file:"+destination);

        return success;

    }

    /**
     * Unzip a directory(even inside .jar) in another.
     *
     * @param filePath
     * @param destination
     * @throws IOException
     */
    public static void unzip(Path filePath, Path destination) throws IOException {
        //Path filePath = Paths.get( zipFilePath );
        Map<String, String> zipProperties = new HashMap<>();
        /* We want to read an existing ZIP File, so we set this to False */
        zipProperties.put("create", "false");
        zipProperties.put("encoding", "UTF-8");
        URI zipFile = URI.create("jar:file:" + filePath.toUri().getPath());

        try (FileSystem zipfs = FileSystems.newFileSystem(zipFile, zipProperties)) {
            Path rootPath = zipfs.getPath("/");
            Files.walkFileTree(rootPath, new SimpleFileVisitor<Path>() {

                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {

                    Path targetPath = destination.resolve(rootPath.relativize(dir).toString());
                    if (!Files.exists(targetPath)) {
                        Files.createDirectory(targetPath);
                    }
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

                    Files.copy(file, destination.resolve(rootPath.relativize(file).toString()), StandardCopyOption.COPY_ATTRIBUTES, StandardCopyOption.REPLACE_EXISTING);
                    return FileVisitResult.CONTINUE;
                }
            });
        }
    }

}
