/*
 */
package it.mariano.mvnmamachine;

import it.mariano.malog.MaLog;
import it.mariano.mvnmamachine.client.Client;
import it.mariano.mvnmamachine.server.Server;
import it.mariano.mvnmamachine.utility.Ut;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author maria
 */
public class MvnMaMachine {

    public static Properties appProp = new Properties();

    private MaLog malog = new MaLog(getClass());

    {
        malog.setLevel(MaLog.LEVELS.VERBOSE);
    }

    public MvnMaMachine() {

        try {
            appProp.load(Ut.getRisorsa(getClass(), "conf/app.properties"));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MvnMaMachine.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MvnMaMachine.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(-1);
        }

        /*
          Scan APP Properties.
          Start Server if server=true
         */
        if (appProp.getProperty("server", "false").toLowerCase().contains("true")) {
            try {
                new Server().goUp();
            } catch (IOException ex) {
                Logger.getLogger(MvnMaMachine.class.getName()).log(Level.SEVERE, null, ex);
                malog.wtf("Il server non parte...necessita intervento tecnico!");
                System.exit(-200);
            }
        }
        /*
          start a loop to open clients
         */
        int maxClient = Integer.parseInt(appProp.getProperty("max_client", "100"));
        for (int ciclo = 1; ciclo < maxClient; ciclo++) {
            if (appProp.getProperty("client_" + ciclo, "false").toLowerCase().contains("true")) {
                try {
                    new Client(ciclo).goUp();
                } catch (IOException ex) {
                    Logger.getLogger(MvnMaMachine.class.getName()).log(Level.SEVERE, null, ex);
                    malog.wtf("Il client con ID:" + ciclo + "non parte...necessita intervento tecnico");
                    System.exit(-300);
                }
            }
        }
    }

    static public void main(String[] args) {
        MvnMaMachine mm = new MvnMaMachine();
//        Server.main(null);
//        Client.main(null);
    }
}
