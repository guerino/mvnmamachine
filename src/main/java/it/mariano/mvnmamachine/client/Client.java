/*
 */
package it.mariano.mvnmamachine.client;

import it.mariano.malog.MaLog;
import it.mariano.mvnmamachine.MvnMaMachine;
import it.mariano.mvnmamachine.utility.Ut;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author maria
 */
public class Client {

    int idClient;

    MaLog malog = new MaLog(getClass());

    {
        malog.setLevel(MaLog.LEVELS.VERBOSE);
    }
    static ExecutorService EXECUTOR_CLIENT = Executors.newFixedThreadPool(
                Integer.parseInt(MvnMaMachine.appProp.getProperty("max_client","100")));

    private final String CLIENT_CONF_NAME;
    Properties prop = new Properties();

    /**
     * Client Constructor
     *
     * @param idClient
     * @throws IOException
     */
    public Client(int idClient) throws IOException {
                
        CLIENT_CONF_NAME = "conf/client_" + idClient + ".properties";
        this.idClient = idClient;
        prop.load(Ut.getRisorsa(getClass(), CLIENT_CONF_NAME));
    }

    public Client goUp() {
        EXECUTOR_CLIENT.execute(new thClient());
        return this;
    }

    private class thClient implements Runnable {

        @Override
        public void run() {
            for (;;) {
                try (
                        Socket socket = new Socket(prop.getProperty("host", "127,0,0,1"), Integer.parseInt(prop.getProperty("port", "6363")));
                        DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
                        DataInputStream dis = new DataInputStream(socket.getInputStream());) {
                    /**
                     * !!!PROTOCOLLO INTESTAZIONE!!!
                     *
                     * Tutti i dati intestazione pacchetto protocollo (id)(per
                     * ora mi serve solo l' id del client)
                     */
                    dos.writeUTF(idClient+"");

                    /**
                     * fine dati intestazione client
                     */
                    for (;;) {
                        /**
                         * !!!PROTOCOLLO LOOPING!!!
                         *
                         */
                        String fromServer = dis.readUTF();
                        malog.verbose("From Server:" + fromServer);
                        dos.writeUTF("ACK");
                    }
                } catch (IOException ex) {
                    Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
                }
                try {
                    Thread.sleep(Integer.parseInt(prop.getProperty("retry_delay_seconds", "5")) * 1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
                    return;
                }
            }
        }
    }

    public static void main(String[] args) {
        try {
//            Server server = new Server().goUp();            
            new Client(1).goUp();

        } catch (IOException ex) {
            Logger.getLogger(Client.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }
}
